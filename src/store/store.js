import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        total: 0,
        allData: [],
        allPrice: 0,
        shopData:[]
    },
    mutations: {
        getData: (state, data) => {
            data.categoryList.forEach((item) => {
                item.spuList.map((ite) => {
                    return ite.count = 0;
                })

            });
            state.allData = data;
        },
        changeDatas: (state, data) => {
            var { smallind, bidInd } = {...data};
            state.allData.categoryList[bidInd].spuList.map((item) => {
                if (item.spuId == smallind) {
                    if (item.count < 1) {
                        item.count = 1;
                        state.total++;
                        state.allPrice += item.originPrice;
                        if(item.count >= 1){
                            state.shopData.push(item)
                            console.log(state.shopData)
                        }
                    }
                }
            });
        },
        changecount(state, data) {
            var { type, smallind, bidInd } = {...data }
            if(bidInd !== undefined){
                state.allData.categoryList[bidInd].spuList.map((item) => {
                    if (item.spuId == smallind) {
                        if (type === "add") {
                            console.log(1)
                            item.count++;
                            state.allPrice += item.originPrice
                            state.total++;
                        } else {
                            item.count--;
                            state.allPrice -= item.originPrice
                            state.total--;
                            if (item.count < 1){
                                state.shopData.shift(item)
                                console.log(state.shopData)
                            }
                        }
                    }
                });
            }else{
                state.shopData.forEach((item)=>{
                    if (item.spuId == smallind) {
                        if (type === "add") {
                            console.log(1)
                            item.count++;
                            state.allPrice += item.originPrice
                            state.total++;
                        } else {
                            item.count--;
                            state.allPrice -= item.originPrice
                            state.total--;
                            if (item.count < 1){
                                state.shopData.shift(item)
                                console.log(state.shopData)
                            }
                        }
                    }
                })
            }
        }
    },
    actions: {
        getData(context, data) {
            console.log(data)
            context.commit("getData", data)
        },
        changeCount(context, data1) {
            context.commit("changeDatas", data1)
        },
        changeCounts(context, data1) {
            context.commit("changecount", data1)
        }
    }
})